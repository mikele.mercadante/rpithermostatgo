module RPiThermostatGo

go 1.13

require (
	github.com/gin-gonic/gin v1.6.2
	github.com/golang/protobuf v1.3.5 // indirect
	github.com/mattn/go-sqlite3 v2.0.3+incompatible
	github.com/modern-go/concurrent v0.0.0-20180306012644-bacd9c7ef1dd // indirect
	github.com/modern-go/reflect2 v1.0.1 // indirect
	github.com/stretchr/testify v1.5.1
	golang.org/x/sys v0.0.0-20200327173247-9dae0f8f5775 // indirect
	periph.io/x/periph v3.6.2+incompatible
)
